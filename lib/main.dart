import 'package:flutter/material.dart';

void main() {
  runApp(ContactprofilePage());
}
class ContactprofilePage extends StatelessWidget {
  @override
  Widget build(BuildContext context){
    return MaterialApp(
      debugShowCheckedModeBanner:  false,
      home: scafford(
        appBar: Appbar(
          backgroundColor: Colors.cyonAccent,
        leading: Icon(
          Icon.arrow_back,
          color: Colors.black,
        ),
          actions: <Widget>[
            IconButton(
                onPresses: (){},
                icon: Icon(Icons.star_border),
                color: Colors.black,
            )
          ],
        ), // App bar
        body: ListView(
          children: <Widget>[
            Column(
                children: <Widget>[
                  Container(
                    width: double.infinity,
                    height: 250,
                    child: Image.network(
                      "https://github.com/ptyagicodecamp"
                          fit: BoxFit.cover,
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(top:8, bottom:8)
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,

                      children: <Widget>[
                        Padding(
                          child: Text("Priyanka Tyagi",
                          style: TextStyle(fontSize:30),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Divider(
                    color: Colors.grey,
                  ),
                ],
            ),
          ],
        ),
      ),
    );
  }
}
Widget buildCallButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.call,
          color: Colors.indigo.shade800,
        ),
        onPressed: () {},
      ),
      Text("Call"),
    ],
  );
}